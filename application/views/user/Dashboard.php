<style>
.container {
  color: #333;
  text-align: center;
}

h1 {
  font-weight: normal;
}

li {
  display: inline-block;
  font-size: 1.5em;
  list-style-type: none;
  padding: 1em;
  text-transform: uppercase;
}

li span {
  display: block;
  font-size: 4.5rem;
}

/*Css End for timer*/

.card {
    box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);
    background: #fdfdfd;
    border-radius: 5px;
    min-height: 136px;
    border-right: 2px solid #fdfdfd;
}
.card [class*="card-header-"] .card-icon, .card [class*="card-header-"] .card-text {
    border-radius: 1px;
    background-color: #999999;
    padding: 15px;
    margin-top: -20px;
     margin-right: 0px;
     float: none;
    text-align: left;
    /* margin: 0px !important; */
}
.card .card-footer {
    display: block;
    align-items: center;
    background-color: transparent;
    border: 0;
}
.card .card-category:not([class*="text-"]) {
    /* color: #999999; */
    font-size: 16px;
}
.card .card-title {
    margin-top: 0;
    margin-bottom: 15px;
    font-size: 35px !important;
    color: #333;
}
.card-stats .card-header+.card-footer {
    /*border-top: 1px solid #3a487e;*/
    margin-top: 14px;
}
.time{
  height: 80px;width: 80px;border-right: 4px solid #c51162;border-radius: 100%;font-size: 44px;line-height: 1.8;color: #333;font-weight: 300;border-bottom: 4px solid #c51162;
}
.day{
  color: #333;
  font-size: 16px;
  padding-top: 8px;
}
.title{
  font-weight: normal;
    font-size: 27px;
    text-transform: uppercase;
    color: #333;
}
.mt-30{
  margin-top: -30px;
}
.mt-50{
  margin-top: -50px;
}
.nav-item{
  padding: 0px;
  width: 100%;

}

/*.sidebar .logo {
    padding: 15px 0px;
    margin: 0;
    display: block;
    position: relative;
    z-index: 4;
    background: #a7034d;
    color: #fdfdfd;
}*/
.active{
  background: none;
}

.card .card-category:not([class*="text-"]) {
    /* color: #999999; */
    font-size: 16px;
    font-weight: 500;
    color: #333;
    letter-spacing: 1.5px;
    margin-bottom: 4px;
}
.size-19{
  font-size: 19px;
}
</style>
      <!-- End Navbar -->
      <div class="content">
        <div class="container mt-30" style="background: #fdfdfd;
    margin-bottom: 30px;
    padding-top: 30px;
    width: 97.5%;border-radius: 5px;">
          <h1 class="title" id="head"><span style="color: #c51162;">Countdown</span> for the current Phase</h1>
          <p class="size-19">Get <?=$phase->percentage_gain?>% bonus on purchase of token in this phase</p>
          <hr style="width: 50%;">
          <ul>
            <li><span id="days" class="time"></span>
              <p class="day">days</p></li>
            <li><span id="hours" class="time"></span><p class="day">Hours</p></li>
            <li><span id="minutes" class="time"></span><p class="day">Minutes</p></li>
            <li><span id="seconds" class="time"></span><p class="day">Seconds</p></li>
          </ul>
        </div>
        <div class="container-fluid mt-50">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
              <div class="card card-stats">
               <!--  <div class="card-header card-header-warning card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">content_copy</i>
                  </div>
                </div> -->
                <div class="card-footer">
                  <div class="right" style="min-height: 50px;text-align: center;float: none;">
                  <img src="<?=base_url('public/user')?>/img/balance (1).svg" style="height: 60px;width: 60px;">
                </div>
                  <div class="left" style="text-align: center;">
                  <p class="card-category white">Ether Balance</p>
                  <h3 class="card-title"><?=number_format((float)$ether_balance, 6, '.', '')?></h3>
                </div>

                </div>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
              <div class="card card-stats">
               <!--  <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons white">money</i>
                  </div>

                </div> -->
                <div class="card-footer">
                   <div class="right" style="min-height: 50px;float: none;text-align: center;">
                  <img src="<?=base_url('public/user')?>/img/token.svg" style="height: 60px;width: 60px;">
                </div>
                  <div class="left" style="text-align: center;">
                   <p class="card-category">Token</p>
                  <h3 class="card-title"><?=number_format((float)$token, 2, '.', '')?></h3>
                </div>

                </div>
              </div>
            </div>


            <?php
              // echo $phase->start_date;
              // echo $phase->end_date;
              $today = date('Y-m-d h:m:i');
              $start_date = new DateTime($today);
          
              $since_start = $start_date->diff(new DateTime($phase->end_date));
               $since_start->d.' days<br>';
               $since_start->h.' hours<br>';
               $since_start->i.' minutes<br>';
               $since_start->s.' seconds<br>';

              $date=date_create($phase->end_date);
              $format =  date_format($date,"M d, Y H:i:s");
            ?>

          </div>
          </div>
        </div>
<script type="text/javascript">
  const second = 1000,
      minute = second * 60,
      hour = minute * 60,
      day = hour * 24;

let countDown = new Date('<?=$format?>').getTime(),
    x = setInterval(function() {

      let now = new Date().getTime(),
          distance = countDown - now;

      document.getElementById('days').innerText = Math.floor(distance / (day)),
        document.getElementById('hours').innerText = Math.floor((distance % (day)) / (hour)),
        document.getElementById('minutes').innerText = Math.floor((distance % (hour)) / (minute)),
        document.getElementById('seconds').innerText = Math.floor((distance % (minute)) / second);

      //do something later when date is reached
      //if (distance < 0) {
      //  clearInterval(x);
      //  'IT'S MY BIRTHDAY!;
      //}

    }, second)
</script>
