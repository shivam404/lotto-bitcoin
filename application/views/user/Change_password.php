<style>
.card [class*="card-header-"]:not(.card-header-icon):not(.card-header-text):not(.card-header-image) {
    border-radius: 1px;
    margin-top: -20px;
    padding: 15px;
}
.card .card-header .card-title {
    margin-bottom: 3px;
    text-transform: uppercase;
    font-size: 20px;
    text-align: center;
}
.card {
    box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);
    background: #fdfdfd;
    border-radius: 5px;
    min-height: 136px;
    /*border-right: 2px solid #fdfdfd;*/
}
.form-control {
    background: no-repeat center bottom, center calc(100% - 1px);
    background-size: 0 100%, 100% 100%;
    border: 1px solid #333;
    height: 36px;
    transition: background 0s ease-out;
    padding-left: 18px;
    padding-right: 0;
    border-radius: 25px;
    font-size: 14px;
}
.form-control:invalid {
    background-image: none;
}
.card .card-title {
    margin-top: 0;
    margin-bottom: 15px;
    text-align: center;
    color: #333;
    font-size: 21px;
    padding-top: 20px;
    text-transform: uppercase;
}
</style>

<div class="content">
  <div class="row">
    <div class="col-md-8" style="margin: auto;">
    <div class="container-fluid">
      <div class="card">
       <!--  <div class="card-header card-header-primary">
          
        </div> -->
        <div class="card-body">
          <h3 class="card-title"><span style="color: #c51162">Change</span> Password</h3>
          <form action="<?=base_url('user/change_password')?>" method="post">
            <div class="row">
              <div class="col-md-7" style="margin:auto;">
                <div class="form-group bmd-form-group">
                  <input type="password" class="form-control" id="token_price"  placeholder="Old password" name="old_pass" required>
                </div>
                <div class="form-group bmd-form-group">
                  <input type="password" class="form-control" id="token_price"  placeholder="New password" name="new_pass" required>
                </div>
                <div class="form-group bmd-form-group">
                  <input type="password" class="form-control" id="token_price"  placeholder="Confirm New password" name="confirm_pass" required>
                </div>
              </div>

              <div class="col-md-7" style="margin: auto;">
                <div class="form-group bmd-form-group">
                  <input type="submit" class="btn btn-primary" name="submit" value="submit" style="width: 100%;border-radius: 25px; background: #C51162;">
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div>
    </div>
  </div>
