<style>
.card [class*="card-header-"]:not(.card-header-icon):not(.card-header-text):not(.card-header-image) {
    border-radius: 1px;
    margin-top: -20px;
    padding: 15px;
}
.card .card-header .card-title {
    margin-bottom: 3px;
    text-transform: uppercase;
    font-size: 20px;
    text-align: center;
}
.card {
    box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);
    background: #fdfdfd;
    border-radius: 0px;
    min-height: 136px;
    /*border-right: 2px solid #fdfdfd;*/
}
/*.form-control {
    background: no-repeat center bottom, center calc(100% - 1px);
    background-size: 0 100%, 100% 100%;
    border: 1px solid #;
    height: 36px;
    transition: background 0s ease-out;
    padding-left: 18px;
    padding-right: 0;
    border-radius: 25px;
    font-size: 14px;
    
}*/
.form-control:invalid {
    background-image: none;
}
.card .card-title {
    margin-top: 0;
    margin-bottom: 15px;
    text-align: center;
    color: #333;
    font-size: 21px;
    padding-top: 20px;
}
.card .card-body {
    padding: 0.9375rem 20px;
    position: relative;
    padding-bottom: 37px;
}
.card .card-title {
    margin-top: 0;
    margin-bottom: 15px;
    text-align: center;
    color: #333;
    font-size: 21px;
    padding-top: 20px;
}
</style>

<div class="content">
  <div class="row">
    <div class="col-md-8" style="margin: auto;">
    <div class="container-fluid">
      <div class="card">
       <!--  <div class="card-header card-header-primary">
          <h3 class="card-title">Send Token</h3>
        </div> -->
        <div class="card-body">
          <h3 class="card-title"><span style="color: #c51162">SEND</span> TOKEN</h3>
          <form action="<?=base_url('user/send_token')?>" method="post">
          <div class="row">
            <div class="col-md-8" style="margin:auto;">
              <div class="form-group bmd-form-group">
                <input type="text" class="form-control" placeholder="Address" name="address" required>
              </div>
            </div>
            <div class="col-md-8" style="margin:auto;">
              <div class="form-group bmd-form-group">
                <input type="number" class="form-control" placeholder="Token Amount" name="token" required>
                <input type="submit" class="btn btn-primary pull-right" name="submit" value="submit" style="width: 100%;margin-top: 30px;border-radius: 25px; background: #C51162;">
              </div>
            </div>
          </div>
        </form>
        </div>
      </div>
    </div>
    <div>
    </div>
  </div>
