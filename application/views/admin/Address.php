<style>
.sort{
  list-style: none;
  border: 1px #ccc solid;
  color:#333;
  padding: 10px;
  margin-bottom: 4px;
}
</style>
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <?=$this->session->flashdata('error_msg');?>
          
        </div>
      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
    </div>
  </div>
  <div class="clearfix"></div>

  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
         <!--  <a href="" class="btn btn-danger btn-xs m-b-10 m-l-5"><i class="fa fa-plus"></i>&nbsp;category</a>
          
          <a href="" class="btn btn-success btn-xs m-b-10 m-l-5"><i class="fa fa-arrow-left"></i>&nbsp;back</a>
        -->
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <table id="datatable" class="table table-striped">
            <thead style="background: rgba(52, 73, 94, 0.94);color: #ECF0F1;">
              <tr>
                <th>S.No</th>
                <th>Name</th>
                
                <th>Email</th>
              
               <th>Ether Address</th>
              </tr>
            </thead>
            <tbody>
              <?php $i=0;foreach ($rows as  $row) { $i++;?>
                <tr>
                  <td><?=$i?></td>
                  <td><?=$row->name?></td>
                  <td><?=$row->email?></td>
                  <td><?=$row->eth_add?></td>
                  
                  
                  </tr>
                  <?php } ?>
              
                <tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">

function delete_confirm(id){
    swal({
            title: "Are you sure to delete ?",
            text: "You will not be able to recover this !!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it !!",
            cancelButtonText: "No, cancel it !!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){
            if (isConfirm) {
                window.location.href="<?=base_url('category/delete/')?>"+id;
                 // swal("Deleted !!", id, "success");
            }
            else {
                swal("Cancelled !!", "", "error");
            }
        });
};
</script>
