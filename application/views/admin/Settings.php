<style>
.sort{
  list-style: none;
  border: 1px #ccc solid;
  color:#333;
  padding: 10px;
  margin-bottom: 4px;
}
</style>
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <?=$this->session->flashdata('error_msg');?>

      </div>
      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
        </div>
      </div>
      <div class="clearfix"></div>

      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <!--  <a href="" class="btn btn-danger btn-xs m-b-10 m-l-5"><i class="fa fa-plus"></i>&nbsp;category</a>

              <a href="" class="btn btn-success btn-xs m-b-10 m-l-5"><i class="fa fa-arrow-left"></i>&nbsp;back</a>
            -->
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Use changelly?<small></small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <form action="<?=base_url('admin/rate/')?>" method="post">
                      <div class="form-group">
                        <div class="checkbox">
                            <label>
                              <input class="checkboxed" name="check[]" type="radio" value="1" <?=($return->result()[0]->status == 1) ? 'checked':''?>> Yes
                            </label>
                          </div>
                          <div class="checkbox">
                              <label>
                                <input class="checkboxed" name="check[]" type="radio" value="0"  <?=($return->result()[0]->status == 0) ? 'checked':''?>> No
                              </label>
                            </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<script type="text/javascript">
$(".checkboxed").change(function() {
    if(this.checked) {
        //Do stuff
        var check_value = $(this).val();
        $.ajax({
          url: '<?=base_url('admin/setting_process')?>',
          type:'POST',
          data:{check:check_value},
          success:function(data){
            console.log(data);
          }
        });
    }
});
</script>
