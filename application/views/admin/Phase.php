<style>
.sort{
  list-style: none;
  border: 1px #ccc solid;
  color:#333;
  padding: 10px;
  margin-bottom: 4px;
}
</style>
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <?=$this->session->flashdata('error_msg');?>
           <?=$this->session->flashdata('item');?>
      </div>
      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
        </div>
      </div>
      <div class="clearfix"></div>

      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <!--  <a href="" class="btn btn-danger btn-xs m-b-10 m-l-5"><i class="fa fa-plus"></i>&nbsp;category</a>

              <a href="" class="btn btn-success btn-xs m-b-10 m-l-5"><i class="fa fa-arrow-left"></i>&nbsp;back</a>
            -->
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-offset-3 col-md-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Set Token Phase<small></small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <form action="<?=base_url('admin/phase/')?>" method="POST">
                      <div class="form-group">
                        <input type="text" required="required" name="name" placeholder="Enter name" class="form-control col-md-8 col-xs-8" value="<?=!empty($ret->name)? $ret->name : '' ?>" style="margin-bottom: 10px;">
                        <?php echo form_error("name");?>
                        <input type="hidden" required="required" name="id" placeholder="Enter name" class="form-control col-md-8 col-xs-8"  value="<?=!empty($ret->id)? $ret->id : '' ?>">

                      </div>

                     <div class="form-group">
                        <div class='input-group date' id='myDatepicker'>
                            <input type='text' class="form-control" name="s_date" placeholder="Enter Start date" value="<?=!empty($ret->start_date)? $ret->start_date : '' ?>"/>
                            <span class="input-group-addon">
                               <span class="glyphicon glyphicon-calendar"></span>
                            </span>

                        </div>
                         <?php echo form_error("s_date");?>
                    </div>
                      <div class="form-group">
                        <div class='input-group date' id='myDatepicker1'>
                            <input type='text' class="form-control" name="e_date" placeholder="Enter End date" value="<?=!empty($ret->end_date)? $ret->end_date : '' ?>"/>
                            <span class="input-group-addon">
                               <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                        <?php echo form_error("l_date");?>
                    </div>

                      <div class="form-group">
                        <input type="text" required="required" name="percentage_gain"  placeholder="Enter Percentage Gain" class="form-control col-md-8 col-xs-8" value="<?=!empty($ret->percentage_gain)? $ret->percentage_gain : '' ?>" style="margin-bottom: 10px;">
                        <?php echo form_error("percentage_gain");?>
                        <input type="submit" class="btn btn-primary" name="submit" value="save">
                      </div>


                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<script type="text/javascript">

function delete_confirm(id){
  swal({
    title: "Are you sure to delete ?",
    text: "You will not be able to recover this !!",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes, delete it !!",
    cancelButtonText: "No, cancel it !!",
    closeOnConfirm: false,
    closeOnCancel: false
  },
  function(isConfirm){
    if (isConfirm) {
      window.location.href="<?=base_url('category/delete/')?>"+id;
      // swal("Deleted !!", id, "success");
    }
    else {
      swal("Cancelled !!", "", "error");
    }
  });
};
</script>
