<?php

class User extends CI_Controller
{
  function  __construct()
  {
    parent::__construct();
    define('SALT','thisisshivam');
    define ('COIN_URL','http://localhost:3002/');
    define('ADMIN_ADD','0xF27b94e299f61CA3582F1e9D228d687647afB506');
  }
  function logout()
  {
    session_destroy();
    redirect(base_url('user/login'));
  }


  function _transfer_eth($from,$value)
  {
    $to = ADMIN_ADD;
    $password = 'testing';
    $value = $value;

    $url = COIN_URL."api/sendEtherTransaction";
    $ch = curl_init($url);
    curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
    curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
    curl_setopt($ch,CURLOPT_POST,1);
    curl_setopt($ch,CURLOPT_POSTFIELDS,"from=$from&to=$to&value=$value&password=$password");
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
    $output = curl_exec($ch);
    curl_close($ch);
    $output = json_decode($output);
    return $output;
  }
  function change_password()
  {
    user_auth();
    $submit = $this->input->post('submit',true);
    if($submit == 'submit')
    {
      $user_id = $_SESSION['user_id'];
      $old_password = $this->_password($this->input->post('old_pass',true));
      $new_password = $this->input->post('new_pass',true);
      $confirm_new_password = $this->input->post('confirm_pass',true);

      if($new_password != $confirm_new_password)
      {
        $this->_redirect_with_error('Password and confirm password did not match', base_url('user/change_password'));
      }

      $user_return = $this->_custom_query("select * from user where id=$user_id");
      $db_old_password = $user_return->result()[0]->password;

      if($old_password == $db_old_password)
      {
        $change_pass = $this->_password($new_password);
        $this->_custom_query("update user set password='$change_pass' where id = $user_id");
        $this->_redirect_with_error('Password changed successfully', base_url('user/change_password'));
      }
      else
      {
        $this->_redirect_with_error('Password did not match with your old password', base_url('user/change_password'));
      }
    }
    $data['title'] = 'Purchase Token';
    $this->load->view('user/Header',$data);
    $this->load->view('user/Change_password');
    $this->load->view('user/Footer');
  }
  function _check_changelly_status()
  {
    $return = $this->_custom_query("select * from setting order by id desc limit 1");
    return $return->result()[0]->status;
  }
  function purchase_token()
  {
    user_auth();
    $submit = $this->input->post('submit',true);

    if($submit == 'Purchase')
    {
      $user_id = $_SESSION['user_id'];

      $amount = $this->input->post('token',true);
      $type   = $this->input->post('type',true);
      $token  = $amount;
      //get all btc_address
      $return = $this->get_exchange_amount(true);

      $to = 'ETH';
      $eth_val = $return[$to];
      $from_val = $return[$type];


      $address = '0x1caed026f281c7b98af778df2d5446a88f7de64d';
      $method = 'createTransaction';

      if($type != 'eth')
      {
        $from = $type;
        //this is for btc transaction
        //check minimum exchange amount at changelly
        $min_method = 'getMinAmount';
        $response = json_decode($this->api_request($min_method,$from,$to,$amount,$address,''));
        $amount_dec = $amount*1/$from_val*1/$eth_val;
        $amount_change = $response->result;

        if($amount_dec <  $amount_change)
        {
          $method = 'getExchangeAmount';
          $from = $type;
          $to='eth';
          $response = json_decode($this->api_request($method,$from,$to,$amount_change,$address,''));
          $res_result =  $response->result;
          $min_token = floor(($eth_val * $res_result) + ($eth_val * $res_result*10/100));
          $this->_redirect_with_error('minimum purchase token for BTC is'.$min_token, base_url('user/purchase_token'));
        }

        $amount = $amount*1/$from_val*1/$eth_val;
        $response = $this->api_request($method,$from,$to,$amount,$address);

        $response = json_decode($response);

        $btc_address = $response->result->payinAddress;
        $return_id = $response->result->id;

        $this->_custom_query("insert into transaction (user_id,address,type,amount,return_id,token) values ($user_id,'$btc_address','$from',$amount,'$return_id',$token) ");

        $return_changelly = $this->_check_changelly_status();

        $arr = ['type'=>$type,'address'=>$btc_address,'value'=>$amount];

        $data['purchase_value'] = $arr;
      }
      elseif($type == 'eth')
      {
        //this is for btc transaction
        $address = $this->_custom_query("select * from address where user_id = $user_id");
        $eth_address = $address->result()[0]->eth_add;
        $amount = $amount*1/$eth_val;
        $etherbal = json_decode(file_get_contents("https://api-ropsten.etherscan.io/api?module=account&action=balancemulti&address=$eth_address"),true);
        $etherbal =   $etherbal['result'][0]['balance']/1000000000000000000;

        if($etherbal > $amount)
        {
          $return = $this->_transfer_eth($eth_address,$amount);
          if(!empty($return->txnHash))
          {
            $today = date('Y-m-d h:m:i');
            $phase_query = $this->_custom_query("SELECT 	percentage_gain	 FROM phase WHERE start_date BETWEEN '$today' AND end_date <= '$today' limit 1");
            $phase = $phase_query->result()[0]->percentage_gain;
            $token = ceil($token + ($phase*100*1/$token));

            $return = $this->_transferTo($eth_address,$token);

            if(!empty($return->txn))
            {
              $txn_hash = $return->txn;
              $this->_custom_query("insert into transaction (user_id,address,type,amount,return_id,token,txn_hash,status) values ($user_id,'$ltc_address','ETH',$amount,'$return_id',$token,'$txn_hash',1) ");
              $this->_redirect_with_error('Transaction successfully done', base_url('user/purchase_token'));
            }
            else
            {
              $this->_redirect_with_error('Transaction Failed', base_url('user/purchase_token'));
            }
          }
          else
          {
            $this->_redirect_with_error('Transaction Failed', base_url('user/purchase_token'));
          }
        }
        else
        {
          $this->_redirect_with_error('You do not have enough ether balance to make this transaction', base_url('user/purchase_token'));
        }
      }
      // elseif($type == 3)
      // {
      //   //this is for ltc Transaction
      //   $from = 'ltc';

      //   $min_method = 'getMinAmount';
      //   $response = json_decode($this->api_request($min_method,$from,$to,$amount,$address,''));
      //   $amount_dec = $amount*1/$ltc_val*1/$eth_val;
      //   $amount_change = $response->result;

      //   if($amount_dec <  $amount_change)
      //   {
      //     $method = 'getExchangeAmount';
      //     $from = 'ltc';
      //     $to='eth';
      //     $response = json_decode($this->api_request($method,$from,$to,$amount_change,$address,''));
      //     $res_result =  $response->result;
      //     $min_token = floor(($eth_val * $res_result) + ($eth_val * $res_result*10/100));

      //     $this->_redirect_with_error('minimum purchase token for LTC is '.$min_token, base_url('user/purchase_token'));
      //   }
      //   $amount = $amount*1/$ltc_val*1/$eth_val;
      //   $response = $this->api_request($method,$from,$to,$amount,$address,'');
      //   $response = json_decode($response);
      //   $ltc_address = $response->result->payinAddress;
      //   $return_id = $response->result->id;
      //   //save in database
      //   $this->_custom_query("insert into transaction (user_id,address,type,amount,return_id,token) values ($user_id,'$ltc_address','LTC',$amount,'$return_id',$token) ");

      //   $return_changelly = $this->_check_changelly_status();
      //   if($return_changelly == 1)
      //   {
      //     $return_ltc = $this->_custom_query("select * from address where id = 1");
      //     $ltc_address = $return_ltc->result()[0]->ltc_add;
      //     $arr = ['type'=>'LTC','address'=>$ltc_address,'value'=>$amount];
      //   }
      //   else
      //   {
      //     $arr = ['type'=>'LTC','address'=>$ltc_address,'value'=>$amount];
      //   }
      //   $data['purchase_value'] = $arr;
      // }
      // elseif($type == 4)
      // {
      //   //this is for ltc Transaction
      //   $from = 'zec';

      //   $min_method = 'getMinAmount';
      //   $response = json_decode($this->api_request($min_method,$from,$to,$amount,$address,''));
      //   $amount_dec = $amount*1/$zec_val*1/$eth_val;
      //   $amount_change = $response->result;

      //   if($amount_dec <  $amount_change)
      //   {
      //     $method = 'getExchangeAmount';
      //     $from = 'zec';
      //     $to='eth';
      //     $response = json_decode($this->api_request($method,$from,$to,$amount_change,$address,''));
      //     $res_result =  $response->result;
      //     $min_token = floor(($eth_val * $res_result) + ($eth_val * $res_result*10/100));
      //     $this->_redirect_with_error('minimum purchase token for ZEC is '.$min_token, base_url('user/purchase_token'));
      //     exit();
      //   }
      //   $amount = $amount*1/$zec_val*1/$eth_val;
      //   $response = $this->api_request($method,$from,$to,$amount,$address,'');
      //   $response = json_decode($response);
      //   $zec_address = $response->result->payinAddress;
      //   $return_id = $response->result->id;
      //   //save in database
      //   $this->_custom_query("insert into transaction (user_id,address,type,amount,return_id,token) values ($user_id,'$zec_address','ZEC',$amount,'$return_id',$token) ");

      //   $arr = ['type'=>'ZEC','address'=>$zec_address,'value'=>$amount];
      //   $data['purchase_value'] = $arr;
      // }
      // elseif($type == 5)
      // {
      //   //this is for ltc Transaction
      //   $from = 'xrp';

      //   $min_method = 'getMinAmount';
      //   $response = json_decode($this->api_request($min_method,$from,$to,$amount,$address,''));
      //   $amount_dec = $amount*1/$xrp_val*1/$eth_val;
      //   $amount_change = $response->result;

      //   if($amount_dec <  $amount_change)
      //   {
      //     $method = 'getExchangeAmount';
      //     $from = 'xrp';
      //     $to='eth';
      //     $response = json_decode($this->api_request($method,$from,$to,$amount_change,$address,''));
      //     $res_result =  $response->result;
      //     $min_token = floor(($eth_val * $res_result) + ($eth_val * $res_result*10/100));
      //     $this->_redirect_with_error('minimum purchase token for XRP is '.$min_token, base_url('user/purchase_token'));
      //   }
      //   $amount = $amount*1/$xrp_val*1/$eth_val;
      //   $response = $this->api_request($method,$from,$to,$amount,$address,'');
      //   $response = json_decode($response);
      //   $xrp_address = $response->result->payinAddress;
      //   $return_id = $response->result->id;
      //   //save in database
      //   $this->_custom_query("insert into transaction (user_id,address,type,amount,return_id,token) values ($user_id,'$xrp_address','XRP',$amount,'$return_id',$token) ");

      //   $arr = ['type'=>'XRP','address'=>$xrp_address,'value'=>$amount];
      //   $data['purchase_value'] = $arr;
      // }
    }
    $data['return_price'] = json_encode($this->get_exchange_amount(true));
    $data['title'] = 'Purchase Token';
    $this->load->view('user/Header',$data);
    $this->load->view('user/Purchase_token');
    $this->load->view('user/Footer');
  }
  function get_exchange_amount($var=false)
  {
    user_auth();
    $eth_query = $this->_custom_query("select * from rate limit 1");
    $eth = $eth_query->result()[0]->eth_value;
    $arrayCoin = ['BTC','ZEC','XRP','ETC','LTC','DOGE','ABYSS','AMP','ANT','ARK','ARN','BAT','BNT','BRD','BTG','CFI','CVC','DCN','DCR','DGB','DNT','EDG','ELF','EXP','GNO','GUP','HMQ','KMD','LBC','LUN','MAID','MCO','MLN','NAV','NBT','NGC','NMR','NOAH','NXT','OMG','PAY','POT','PTOY','QTUM','RCN','REP','RLC','SALT','SNM','STORJ','STR','STX','SWT','SYS','TRST','USDT','VEN','VIB','WAX','XEM','XMO','ZCL','ZEN','ZRX'];
    $eth_array = ['ETH'=>$eth];
    $new_array = array();
    for ($i=0; $i < count($arrayCoin); $i++) {
       array_push($new_array,$this->_get_exchange_price($arrayCoin[$i]));
    }
    $combined_array = array_combine($arrayCoin, $new_array);

    $coin_array = array_merge($combined_array,$eth_array);
    // $btc = $this->_get_exchange_price('btc');
    // $ltc = $this->_get_exchange_price('ltc');
    // $xrp = $this->_get_exchange_price('xrp');
    // $zec = $this->_get_exchange_price('zec');
    // $doge = $this->_get_exchange_price('doge');
    // $abyss = $this->_get_exchange_price('abyss');
    // $amp = $this->_get_exchange_price('amp');

    // $coin_array = ['eth'=>$eth,'btc'=>$btc,'ltc'=>$ltc,'xrp'=>$xrp,'zec'=>$zec,'doge'=>$doge,'abyss'=>$abyss,'amp'=>$amp];
    //check whether $var is true or false
    if($var)
    {
      return $coin_array;
    }
    else
    {
      echo json_encode($coin_array);
    }
  }
  function _get_exchange_price($from)
  {
    $amount = '1';
    $method = 'getExchangeAmount';
    $from = $from;
    $to = 'eth';
    $address = '';
    $id = '';
    $response = $this->api_request($method,$from,$to,$amount,$address,$id);
    $response_json = json_decode($response);
    return $response_json->result;
  }
  function api_request($method,$from,$to,$amount,$address,$id)
  {
    user_auth();
    $apiKey = '837fd8d4650b4d76b6dbfa64a72443b4';
    $apiSecret = '625306acef11f11da805bc8e68f97b3d14977c25d7d158f32bdef70ea4b64525';
    $apiUrl = 'https://api.changelly.com';
    $message = json_encode(
      array('jsonrpc' => '2.0', 'id' => 1, 'method' => $method, 'params' => array('from'=>$from,'to'=>$to,'amount'=>$amount,'address'=>$address,'id'=>$id))
    );
    $sign = hash_hmac('sha512', $message, $apiSecret);
    $requestHeaders = [
      'api-key:' . $apiKey,
      'sign:' . $sign,
      'Content-type: application/json'
    ];
    $ch = curl_init($apiUrl);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $message);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $requestHeaders);

    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
  }
  function send_token()
  {
    user_auth();
    $submit = $this->input->post('submit',true);
    if($submit == 'submit')
    {
      $to= $this->input->post('address',true);
      $token = $this->input->post('token',true);

      $check_if_address_exist_in_db = $this->_custom_query("select * from address where eth_add = '$to'");
      if(count($check_if_address_exist_in_db->result()) == 0)
      {
        $this->_redirect_with_error('This Address does not exist in database', base_url('user/send_token'));
      }

      $user_id = $_SESSION['user_id'];
      $return = $this->_custom_query("select * from address where user_id = $user_id");
      $address = $return->result()[0]->eth_add;

      $etherbal = json_decode(file_get_contents("https://api-ropsten.etherscan.io/api?module=account&action=balancemulti&address=$address"),true);
      $etherbal =   $etherbal['result'][0]['balance']/1000000000000000000;

      if($etherbal > 0.001000)
      {
        $from = $address;
        $password = 'testing';
        $value = $amount.'000000000000000000';
        $url = COIN_URL."api/transferTo";
        $ch = curl_init($url);
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
        curl_setopt($ch,CURLOPT_POST,1);
        curl_setopt($ch,CURLOPT_POSTFIELDS,"from=$from&to=$to&value=$value&password=$password");
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        $output = curl_exec($ch);
        curl_close($ch);
        $output = json_decode($output);
        if($output->txn)
        {
            $this->_redirect_with_error('Transaction successfully completed', base_url('user/send_token'));
        }
        else
        {
            $this->_redirect_with_error('Transaction failed', base_url('user/send_token'));
        }
      }
      else
      {
        $this->_redirect_with_error('you do not have enough ether balance in your account to make this transaction', base_url('user/send_token'));
      }
    }
    $data['title'] = 'Send Token';
    $this->load->view('user/Header',$data);
    $this->load->view('user/Send_token');
    $this->load->view('user/Footer');
  }
  function receive()
  {
    user_auth();
    $user_id = $_SESSION['user_id'];
    $data['address'] = $this->_get_user_address($user_id);
    $data['title'] = 'Receive Token';
    $this->load->view('user/Header',$data);
    $this->load->view('user/Receive');
    $this->load->view('user/Footer');
  }
  function dashboard()
  {
    user_auth();
    $user_id = $_SESSION['user_id'];
    $return = $this->_custom_query("select * from address where user_id = $user_id");
    $address = $return->result()[0]->eth_add;
    $today = date('Y-m-d h:m:i');
    $phase_query = $this->_custom_query("SELECT * FROM phase WHERE start_date BETWEEN '$today' AND end_date <= '$today' limit 1");
    $data['phase'] = $phase_query->row();

    $etherbal = json_decode(file_get_contents("https://api-ropsten.etherscan.io/api?module=account&action=balancemulti&address=$address"),true);
    $etherbal =   $etherbal['result'][0]['balance']/1000000000000000000;

    $url = COIN_URL."api/getBalance";
    $ch = curl_init($url);
    curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
    curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
    curl_setopt($ch,CURLOPT_POST,1);
    curl_setopt($ch,CURLOPT_POSTFIELDS,"ether_address=$address");
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
    $output = curl_exec($ch);
    curl_close($ch);
    $output = json_decode($output);
    $token = $output->balance/1000000000000000000;
    $data['token'] = $token;
    $data['ether_balance'] = $etherbal;

    $data['title'] = 'Dashboard';
    $this->load->view('user/Header',$data);
    $this->load->view('user/Dashboard');
    $this->load->view('user/Footer');
  }
  function _transferTo($to, $amount)
  {
    user_auth();
    $from = ADMIN_ADD;
    $password = 'testing';
    $value = $amount.'000000000000000000';
    $url = COIN_URL."api/transferTo";
    $ch = curl_init($url);
    curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
    curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
    curl_setopt($ch,CURLOPT_POST,1);
    curl_setopt($ch,CURLOPT_POSTFIELDS,"from=$from&to=$to&value=$value&password=$password");
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
    $output = curl_exec($ch);
    curl_close($ch);
    $output = json_decode($output);
    return $output;
  }
  function _get_user_address($user_id)
  {
    $return = $this->_custom_query("select  * from `address` where user_id = $user_id");
    return $return->result()[0]->eth_add;
  }
  function login()
  {
    $submit = $this->input->post('submit',true);
    if(!empty($submit))
    {
      $email = $this->input->post('email', true);
      $password = $this->input->post('password', true);

      if($email=='' OR $password=='')
      {
        $this->_redirect_with_error('Please fill all fields', base_url('user/login'));
      }

      $password = $this->_password($password);
      $return = $this->_custom_query("select * from user where email='$email' and password='$password'");

      if(count($return->result()) == 1)
      {
        if($return->result()[0]->status == 1)
        {
            $this->_redirect_with_error('User blocked by admin', base_url('user/login'));
        }
        else
        {
          $user_id = $return->result()[0]->id;

          $this->session->set_userdata(['user_id'=>$user_id]);
          redirect(base_url('user/dashboard'));
        }
      }
      else
      {
          $this->_redirect_with_error('Password did not match', base_url('user/login'));
      }
    }
    $data['title'] = 'Login';
    $this->load->view('user/Login',$data);
  }
  function signup()
  {
    $submit = $this->input->post('submit',true);
    if($submit == 'submit')
    {
      $first_name = $this->input->post('fname',true);
      $last_name = $this->input->post('lname',true);
      $name = $first_name .' '.$last_name;
      $email = $this->input->post('email',true);
      $password = $this->input->post('password',true);
      $confirm_password = $this->input->post('password_confirmation', true);
      $username = $this->input->post('username', true);
      $affiliate = $this->input->post('affiliate_from', true);

      if($first_name == '' OR $last_name=='' OR $email=='' OR $password=='' OR $confirm_password=='' OR $username=='')
      {
        $this->_redirect_with_error('Plase fill all fields', base_url('user/login#parentHorizontalTab_agile2'));
      }
      if($password != $confirm_password)
      {
        $this->_redirect_with_error('password did not match', base_url('user/login#parentHorizontalTab_agile2'));
      }
      $return = $this->_custom_query("select * from user where email = '$email'");

      if(count($return->result()) == 1)
      {
        $this->_redirect_with_error('email already exist', base_url('user/login#parentHorizontalTab_agile2'));
      }

      $return = $this->_custom_query("select * from user where username = '$username'");

      if(count($return->result()) == 1)
      {
        $this->_redirect_with_error('username already exist', base_url('user/signup#parentHorizontalTab_agile2'));
        exit();
      }
      $password = $this->_password($password);
      $this->_custom_query("insert into user (name,email,username,password,affiliate_code) values ('$name','$email','$username','$password','$affiliate_code')");

      $user_id = $this->db->insert_id();

      $eth = $this->_get_eth_address();
      $ltc = $this->gen_address('ltc');
      $ltc_add = $ltc['address'];
      $ltc_private = $ltc['private'];

      $btc =  $this->gen_address('btc');
      $btc_add = $btc['address'];
      $btc_private = $btc['private'];
        $this->_custom_query("insert into address (user_id,eth_add,btc_add,btc_private,ltc_add,ltc_private) values ($user_id,'$eth','$btc_add','$btc_private','$ltc_add','$ltc_private')");

      $this->session->set_userdata(['user_id'=>$user_id]);

      redirect(base_url('user/dashboard'));
    }
    $data['title'] = 'Signup';
    $this->load->view('user/Signup');
  }
  function _get_eth_address()
  {
    // $token = '0ed7eba50c5c43f3821046a01b510ee0';
    $url = COIN_URL."api/createEtherAccount";
    $ch = curl_init($url);
    curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
    curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
    //curl_setopt($ch,CURLOPT_POST,1);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
    $output = curl_exec($ch);
    curl_close($ch);
    $x = substr(substr($output, 1),0,-1);
    return $x;
  }
  function _password($password = null)
  {
    $password = hash('sha256',$password.SALT);
    return   $password;
  }
  function _redirect_with_error($error,$location)
  {
    echo '<script type="text/javascript">';
    echo 'alert("'.$error.'");';
    echo 'window.location.href = "'.$location.'";';
    echo '</script>';
    exit();
  }
  function _custom_query($query)
  {
    $this->load->model('Api_model');
    return $this->Api_model->_custom_query($query);
  }
  function gen_address($type)
  {
    $token = '0ed7eba50c5c43f3821046a01b510ee0';

    $url = "https://api.blockcypher.com/v1/".$type."/main/addrs?token=".$token."";

    $ch = curl_init($url);
    curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
    curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
    curl_setopt($ch,CURLOPT_POST,1);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
    $ccc = curl_exec($ch);
    curl_close($ch);
    $json = json_decode($ccc, true);
    return $json;
  }
}
