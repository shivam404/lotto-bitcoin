<?php

class Api extends CI_Controller
{
  function  __construct()
  {
    parent::__construct();
    header('Access-Control-Allow-Origin: *');
    define('SALT','thisisshivam');
    define('ADMIN_ADD','0xF27b94e299f61CA3582F1e9D228d687647afB506');
    error_reporting(0);
    define ('COIN_URL','http://localhost:3002/');
  }
  function phase()
  {
    $today = date('Y-m-d h:m:i');
    $phase_query = $this->_custom_query("SELECT 	percentage_gain	 FROM phase WHERE start_date BETWEEN '$today' AND end_date <= '$today' limit 1");
    $phase = $phase_query->result();
    echo  substr(json_encode($phase),1,-1);
  }
  function save_external_transaction()
  {
    $user_id = $this->input->post('user_id',true);
    $to = $this->input->post('address',true);
    $amount = $this->input->post('amount',true);
    $return = $this->_custom_query("select * from address where user_id = $user_id");
    $address = $return->result()[0]->eth_add;

    $etherbal = json_decode(file_get_contents("https://api-ropsten.etherscan.io/api?module=account&action=balancemulti&address=$address"),true);
    $etherbal =   $etherbal['result'][0]['balance']/1000000000000000000;

    if($etherbal > 0.001000)
    {
      $from = $address;
      $password = 'testing';
      $value = $amount.'000000000000000000';
      $url = COIN_URL."api/transferTo";
      $ch = curl_init($url);
      curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
      curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
      curl_setopt($ch,CURLOPT_POST,1);
      curl_setopt($ch,CURLOPT_POSTFIELDS,"from=$from&to=$to&value=$value&password=$password");
      curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
      $output = curl_exec($ch);
      curl_close($ch);
      $output = json_decode($output);
      if($output->txn)
      {
        echo 'transaction successfully completed';
        exit();
      }
      else
      {
        echo 'transaction failed try again';
        exit();
      }
    }
    else
    {
      echo 'you do not have enough ether balance in your account to make this transaction';
      exit();
    }
  }
  function get_token_bal()
  {
    $user_id = $this->input->post('user_id');
    $return = $this->_custom_query("select * from address where user_id = $user_id");
    $address = $return->result()[0]->eth_add;

    $etherbal = json_decode(file_get_contents("https://api-ropsten.etherscan.io/api?module=account&action=balancemulti&address=$address"),true);
    $etherbal =   $etherbal['result'][0]['balance']/1000000000000000000;

    $url = COIN_URL."api/getBalance";
    $ch = curl_init($url);
    curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
    curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
    curl_setopt($ch,CURLOPT_POST,1);
    curl_setopt($ch,CURLOPT_POSTFIELDS,"ether_address=$address");
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
    $output = curl_exec($ch);
    curl_close($ch);
    $output = json_decode($output);
    $token = $output->balance/1000000000000000000;
    echo json_encode(['token'=>$token,'ether_balance'=>$etherbal]);
    //$etherbal =   $etherbal['result'][0]['balance']/1000000000000000000;
    //echo $etherbal;
  }
  function get_ether_address()
  {
    $user_id = $this->input->post('user_id');
    $address = $this->_custom_query("select * from address where user_id = $user_id");
    $eth_address = $address->result()[0]->eth_add;
    echo  json_encode(['eth_address'=>$eth_address]);
  }
  function cron_job()
  {
    $return = $this->_custom_query("select * from transaction where status = 0");
    foreach ($return->result() as $row)
    {
      $return_id = $row->return_id;
      $id = $row->id;
      $token = ceil($row->token);
      $user_id = $row->user_id;
      $return = $this->_get_status($return_id);
      // $return = 'finished';
      if($return == 'finished')
      {
        $return = $this->_custom_query("select * from address where user_id = $user_id");
        $eth_add = $return->result()[0]->eth_add;

        $today = date('Y-m-d h:m:i');
        $phase_query = $this->_custom_query("SELECT 	percentage_gain	 FROM phase WHERE start_date BETWEEN '$today' AND end_date <= '$today' limit 1");
        $phase = $phase_query->result()[0]->percentage_gain;
        $token = ceil($token + ($phase*100*1/$token));

        $return_token = $this->_transferTo($eth_add, $token);
        if(!empty($return_token->txn))
        {
            $txn = $return_token->txn;
            $this->_custom_query("insert into token_transfer (user_id,token,txn_hash,transaction_id) values ($user_id,$token,'$txn',$id)");
            $this->_custom_query("update transaction set status = 1 where id = $id");
            exit();
        }
        else
        {
          exit();
        }
      }
      else
      {
        exit();
      }
    }
  }
  function return_coin()
  {
    $x = ['BTC','ZEC','XRP','ETC','LTC','DOGE','ABYSS','AMP','ANT','ARK','ARN','BAT','BNT','BRD','BTG','CFI','CVC','DCN','DCR','DGB','DNT','EDG','ELF','EXP','GNO','GUP','HMQ','KMD','LBC','LUN','MAID','MCO','MLN','NAV','NBT','NGC','NMR','NOAH','NXT','OMG','PAY','POT','PTOY','QTUM','RCN','REP','RLC','SALT','SNM','STORJ','STR','STX','SWT','SYS','TRX','TRST','USDT','VEN','VIB','WAX','XEM','XMO','ZCL','ZEN','ZRX'];
    return $x;
  }
  function get_exchange_amount($var=false)
  {
    $x = $this->return_coin();
    $eth_query = $this->_custom_query("select * from rate limit 1");
    $eth = $eth_query->result()[0]->eth_value;

    $coin_array = [['ETH'=>$eth]];
    foreach ($x as  $value)
    {
      $re_value = $this->_get_exchange_price($value);
      $temp_arry = [$value=>$re_value];
      array_push($coin_array , $temp_arry);
    }
    // $coin_array = ['eth'=>$eth,'btc'=>$btc,'ltc'=>$ltc,'xrp'=>$xrp,'zec'=>$zec];
    // //check whether $var is true or false
    if($var)
    {
      return $coin_array;
    }
    else
    {
      echo json_encode($coin_array);
    }
  }
  function _get_exchange_price($from)
  {
    $amount = '1';
    $method = 'getExchangeAmount';
    $from = $from;
    $to = 'eth';
    $address = '';

    $response = $this->api_request($method,$from,$to,$amount,$address);
    $response_json = json_decode($response);
    return $response_json->result;
  }
  function api_request($method,$from,$to,$amount,$address,$id)
  {
    $apiKey = '837fd8d4650b4d76b6dbfa64a72443b4';
    $apiSecret = '625306acef11f11da805bc8e68f97b3d14977c25d7d158f32bdef70ea4b64525';
    $apiUrl = 'https://api.changelly.com';
    $message = json_encode(
      array('jsonrpc' => '2.0', 'id' => 1, 'method' => $method, 'params' => array('from'=>$from,'to'=>$to,'amount'=>$amount,'address'=>$address,'id'=>$id))
    );
    $sign = hash_hmac('sha512', $message, $apiSecret);
    $requestHeaders = [
      'api-key:' . $apiKey,
      'sign:' . $sign,
      'Content-type: application/json'
    ];
    $ch = curl_init($apiUrl);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $message);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $requestHeaders);

    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
  }
  function change_password()
  {
    $user_id = $this->input->post('user_id', true);
    $old_password = $this->_password($this->input->post('old_pass',true));
    $new_password = $this->_password($this->input->post('new_pass',true));

    $user_return = $this->_custom_query("select * from user where id=$user_id");
    $db_old_password = $user_return->result()[0]->password;

    if($old_password == $db_old_password)
    {
      $this->_custom_query("update user set password='$new_password' where id = $user_id");
      echo '{"status":200,"message":"password changed successfully"}';
      exit();
    }
    else
    {
      echo '{"status":400,"message":"password did not match"}';
      exit();
    }
  }
  function _check_changelly_status()
  {
    $return = $this->_custom_query("select * from setting order by id desc limit 1");
    return $return->result()[0]->status;
  }
  function purchase_token()
  {
    $user_id = $this->input->post('user_id', true);
    $amount = $this->input->post('amount',true);
    $type = $this->input->post('type',true);

    if($amount == 'undefined')
    {
      echo json_encode(['status'=>400,'message'=>'Token field is empty']);
      exit();
    }
    if($type == 1)
    {
      echo json_encode(['status'=>400,'message'=>'Please select token type']);
      exit();
    }

    $token = $amount;
    //get all btc_address
    $return = $this->get_exchange_amount(true);

    foreach ($return as $key => $value)
    {
      foreach ($value as $key1 => $value1) {
        if($key1 == $type)
        {
          if($value1 == null)
          {
            echo json_encode(['status'=>400,'message'=>'supoort for this coin has stopped for some time']);
            exit();
          }
          $total_val = $value1;
        }
      }
    }

    //eth value from rate
    $rate = $this->_custom_query("select * from rate order by id DESC LIMIT 1");
    $eth_val = $rate->result()[0]->eth_value;

    $to = 'eth';
    $address = '0x1caed026f281c7b98af778df2d5446a88f7de64d';
    $method = 'createTransaction';

    if($type == 'ETH')
    {
      //this is for btc transaction
      $address = $this->_custom_query("select * from address where user_id = $user_id");
      $eth_address = $address->result()[0]->eth_add;

      $amount = $amount*1/$eth_val;
      $etherbal = json_decode(file_get_contents("https://api-ropsten.etherscan.io/api?module=account&action=balancemulti&address=$eth_address"),true);
      $etherbal =   $etherbal['result'][0]['balance']/1000000000000000000;

      if($etherbal > $amount)
      {
        $return_eth_trans = $this->_transfer_eth($eth_address,$amount);

        if(!empty($return_eth_trans->txnHash))
        {
          $today = date('Y-m-d h:m:i');
          $phase_query = $this->_custom_query("SELECT 	percentage_gain	 FROM phase WHERE start_date BETWEEN '$today' AND end_date <= '$today' limit 1");
          $phase = $phase_query->result()[0]->percentage_gain;
          $token = ceil($token + ($phase*100*1/$token));

          $return = $this->_transferTo($eth_address,$token);

          if(!empty($return->txn))
          {
            $txn_hash = $return->txn;
            $this->_custom_query("insert into token_transfer (user_id,token,txn_hash) values ($user_id,$token,'$txn_hash')");
            echo json_encode(['status'=>200,'message'=>'transaction successfully done']);
            exit();
          }
          else
          {
            echo json_encode(['status'=>400,'message'=>'transaction failed']);
            exit();
          }
        }
        else
        {
          echo json_encode(['status'=>400,'message'=>'transaction failed due to ether transfer']);
          exit();
        }
       }
      else
      {
        echo json_encode(['status'=>400,'message'=>'You do not have enough ether balance to make this transaction
        ']);
        exit();
      }
    }
    else
    {
      //this is for ltc Transaction
      $from = $type;

      $min_method = 'getMinAmount';
      $response = json_decode($this->api_request($min_method,$from,$to,$amount,$address));
      $amount_dec = $amount*1/$total_val*1/$eth_val;
      $amount_change = $response->result;

      if($amount_dec <  $amount_change)
      {
        $method = 'getExchangeAmount';
        $from = $type;
        $to='eth';
        $response = json_decode($this->api_request($method,$from,$to,$amount_change,$address));
        $res_result =  $response->result;
        $min_token = floor(($eth_val * $res_result) + ($eth_val * $res_result*10/100));
        echo json_encode(['status'=>400,'message'=>'minimum purchase token for '.$type.' is '.$min_token]);
        exit();
      }

      $amount = $amount*1/$total_val*1/$eth_val;

      $response = $this->api_request($method,$from,$to,$amount,$address);
      $response = json_decode($response);
      $token_address = $response->result->payinAddress;
      $return_id = $response->result->id;
      //save in database
      $this->_custom_query("insert into transaction (user_id,address,type,amount,return_id,token) values ($user_id,'$token_address','$type',$amount,'$return_id',$token) ");

      $arr = ['type'=>$type,'address'=>$token_address,'value'=>$amount];
      echo json_encode($arr);
      exit();
    }
  }
  function get_status($order_id)
  {
    $to = 'eth';
    $address = '0x1caed026f281c7b98af778df2d5446a88f7de64d';
    $method = 'getStatus';
    $id = $order_id;
    $from = 'ltc';
    $response = $this->api_request($method,$from,$to,$amount,$address,$id);
    $response = json_decode($response);
    return $response->result;
  }
  function _transfer_eth($from,$value)
  {
    $to = ADMIN_ADD;
    $password = 'testing';
    $value = $value;

    $url = COIN_URL."api/sendEtherTransaction";
    $ch = curl_init($url);
    curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
    curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
    curl_setopt($ch,CURLOPT_POST,1);
    curl_setopt($ch,CURLOPT_POSTFIELDS,"from=$from&to=$to&value=$value&password=$password");
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
    $output = curl_exec($ch);
    curl_close($ch);
    $output = json_decode($output);
    return $output;
  }
  function _transferTo($to, $amount)
  {
    $from = ADMIN_ADD;
    $password = 'testing';
    $value = $amount.'000000000000000000';
    $url = COIN_URL."api/transferTo";
    $ch = curl_init($url);
    curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
    curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
    curl_setopt($ch,CURLOPT_POST,1);
    curl_setopt($ch,CURLOPT_POSTFIELDS,"from=$from&to=$to&value=$value&password=$password");
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
    $output = curl_exec($ch);
    curl_close($ch);
    $output = json_decode($output);
    return $output;
  }
  function get_rate($website=false)
  {
    $return = $this->_custom_query("select eth_value from `rate`");
    if($website)
    {
      return $return;
    }
    else
    {
      echo substr(json_encode($return->result()),1,-1);
    }
  }
  function login()
  {
    $email = $this->input->post('email', true);
    $password = $this->input->post('password', true);

    if($email=='undefined' OR $password=='undefined')
    {
      echo '{"status":400,"message":"Fill all fields"}';
      exit();
    }

    $password = $this->_password($password);
    $return = $this->_custom_query("select * from user where email='$email' and password='$password'");

    if(count($return->result()) == 1)
    {
      if($return->result()[0]->status == 1)
      {
        echo '{"status":400,"message":"User Blocked by admin"}';
        exit();
      }
      else
      {
        $user_id = $return->result()[0]->id;
        $return = $this->_custom_query("select user.name,user.email,user.username,user.id,address.eth_add from user inner join address on address.user_id = user.id where user.id = $user_id");
        echo substr(json_encode($return->result()),1,-1);
      }
    }
    else
    {
      echo '{"status":400,"message":"email or password did not match"}';
      exit();
    }
  }
  function signup()
  {
    $first_name = $this->input->post('fname', true);
    $last_name = $this->input->post('lname', true);
    $email = $this->input->post('email', true);
    $password = $this->input->post('password', true);
    $confirm_password = $this->input->post('password_confirmation', true);
    $username = $this->input->post('username', true);
    $affiliate = $this->input->post('affiliate_from', true);

    $name = $first_name.' '.$last_name;

    if($first_name == 'undefined' OR $last_name=='undefined' OR $email=='undefined' OR $password=='undefined' OR $confirm_password=='undefined' OR $username=='undefined')
    {
      echo '{"status":400,"message":"Fill all fields"}';
      exit();
    }

    if($password != $confirm_password)
    {
      //header("HTTP/1.1 400");
      echo '{"status":400,"message":"Password did not match"}';
      exit();
    }

    $return = $this->_custom_query("select * from user where email = '$email'");

    if(count($return->result()) == 1)
    {
      //header("HTTP/1.1 400");
      echo '{"status":400,"message":"Email already exist"}';
      exit();
    }

    $return = $this->_custom_query("select * from user where username = '$username'");

    if(count($return->result()) == 1)
    {
      //header("HTTP/1.1 400");
      echo '{"status":400,"message":"Username already exist"}';
      exit();
    }

    $password = $this->_password($password);
    $this->_custom_query("insert into user (name,email,username,password,affiliate_code) values ('$name','$email','$username','$password','$affiliate_code')");
    $user_id = $this->db->insert_id();

    $eth = $this->_get_eth_address();

    $this->_custom_query("insert into address (user_id,eth_add) values ($user_id,'$eth')");

    $return = $this->_custom_query("select id,name,email from user where id = $user_id");
    //header("HTTP/1.1 200");
    echo '{"status":200,"message":"Success"}';
    //echo substr(json_encode($return->result()),1,-1);
  }
  function _password($password = null)
  {
    $password = hash('sha256',$password.SALT);
    return   $password;
  }
  function get_user_address()
  {
    $user_id = $this->input->post('user_id',true);

    $return = $this->_custom_query("select eth_add as eth_address,btc_add as btc_address,ltc_add as ltc_address from address where user_id = $user_id");
    if(count($return->result()) == 1)
    {
      $json = json_encode($return->result());
      echo substr($json,1,-1);
    }
    else
    {
      echo '{"status":400,"message":"User id not found"}';
    }
  }
  function _get_eth_address()
  {
    // $token = '0ed7eba50c5c43f3821046a01b510ee0';
    $url = COIN_URL."api/createEtherAccount";
    $ch = curl_init($url);
    curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
    curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
    //curl_setopt($ch,CURLOPT_POST,1);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
    $output = curl_exec($ch);
    curl_close($ch);
    $x = substr(substr($output, 1),0,-1);
    return $x;
  }
  function _custom_query($query)
  {
    $this->load->model('Api_model');
    return $this->Api_model->_custom_query($query);
  }

}
