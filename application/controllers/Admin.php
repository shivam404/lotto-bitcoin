<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    define('SALT','thisisshivam');
  }
  function phase()
  {
    $id=$this->uri->segment(3);
    $this->load->library('form_validation');
    $submit = $this->input->post('submit', true);
    if ($submit == 'save') {
      $this->form_validation->set_rules('name', 'Name', 'required');
      $this->form_validation->set_rules('s_date', 'Start Date', 'required');
      $this->form_validation->set_rules('e_date', 'Last Date', 'required');
      $this->form_validation->set_rules('percentage_gain','Percentage Gain', 'required');
      if ($this->form_validation->run()) {
        $s_date  = date_format(date_create($this->input->post('s_date', true)),"Y/m/d h:i:s");
        $name    = $this->input->post('name', true);
        $e_date  = date_format(date_create($this->input->post('e_date', true)),"Y/m/d h:i:s");
        $percentage_gain  = $this->input->post('percentage_gain', true);
        $id=$this->input->post('id');
       if(!is_numeric($id)){
          $query = $this->_custom_query("INSERT INTO phase (name, start_date, end_date, percentage_gain) VALUES('$name','$s_date','$e_date','$percentage_gain')");
          $msg = '<div class="alert alert-info"> Phase are save </div>';
          $this->session->set_flashdata('item', $msg);
          redirect(base_url('admin/phase_view'));
        }else{
          $query   = $this->_custom_query("UPDATE phase SET  name='$name',start_date='$s_date', end_date='$e_date', percentage_gain='$percentage_gain' WHERE id='$id' ");
          $msg = '<div class="alert alert-success">Phase are Update,</div>';
          $this->session->set_flashdata('item', $msg);
          redirect(base_url('admin/phase_view'));
        }
      }
    }
    $query=$this->_custom_query("SELECT * FROM phase WHERE id='$id' ");
    $data['ret']=$query->row();
    $data['title'] = 'Phase';
    $this->load->view('admin/includes/Admin_header', $data);
    $this->load->view('admin/Phase');
    $this->load->view('admin/includes/Admin_footer');
  }
  function phase_view()
    {
      $data['return']=$this->_custom_query("SELECT * FROM phase");
      $data['title'] = 'Phase';
      $this->load->view('admin/includes/Admin_header', $data);
      $this->load->view('admin/Phase_view');
      $this->load->view('admin/includes/Admin_footer');
    }

  function phase_delete()
    {
      $id=$this->uri->segment(3);
      $this->_custom_query("DELETE FROM phase WHERE id='$id'");
       $msg = '<div class="alert alert-danger">Phase has been  Delete !</div>';
       $this->session->set_flashdata('item', $msg);
      redirect(base_url('admin/phase_view'));

    }
  function setting_process()
  {
    $check = $this->input->post('check',true);
    if(($check == 0) || ($check == 1))
    {
      $return = $this->_custom_query("update setting set status=$check");
    }
  }
  function setting()
  {
    $data['return'] = $this->_custom_query("select * from setting order by id desc limit 1");
    $data['title'] = 'Setting';
    $this->load->view('admin/includes/Admin_header', $data);
    $this->load->view('admin/Settings');
    $this->load->view('admin/includes/Admin_footer');
  }
  function set_token_price()
  {

    $data['title'] = 'Set token price';
    $this->load->view('admin/includes/Admin_header', $data);
    $this->load->view('admin/Set_token_price');
    $this->load->view('admin/includes/Admin_footer');
  }
  function index()
  {

    $this->load->library('form_validation');

    $submit = $this->input->post('submit', true);
    if ($submit == 'Log in') {
      $this->form_validation->set_rules('username', 'Email', 'trim|required');
      $this->form_validation->set_rules('password', 'password', 'required');
      if ($this->form_validation->run()) {
        $email    = $this->input->post('username', true);
        $password = $this->_password($this->input->post('password', true));
        $query    = $this->_custom_query("SELECT * FROM user WHERE email = 'admin@admin.com' and password = '$password'");
        if ($query->num_rows() < 1) {

          $msg = 'Email or Password did not match';
          $this->session->set_flashdata('item', $msg);
          redirect(base_url('admin'));

          exit();
        }

        else {
          $data = array(
            'id' => $query->row()->id,
            'email' => $query->row()->email

          );
          $this->session->set_userdata($data);

          redirect(base_url('admin/user'));


        }


      }

    }
    $data['flash'] = $this->session->flashdata('item');
    $data['title'] = 'Admin';
    $this->load->view('admin/login/Login', $data);
  }
  function _password($password = null)
  {
    $password = hash('sha256', $password . SALT);
    return $password;
  }

  function _custom_query($query)
  {
    $this->load->model('Admin_m');
    return $this->Admin_m->_custom_query($query);
  }
  public function dashboard()
  {
    $this->check_admin();
    $data['title'] = 'Dashboard';
    $this->load->view('admin/includes/Admin_header', $data);
    $this->load->view('admin/Dashboard');
    $this->load->view('admin/includes/Admin_footer');
  }
  function user()
  {
    $this->check_admin();
    $data['title'] = 'User';
    $query         = $this->_custom_query("SELECT * FROM user");
    $data['rows']  = $query->result();
    $this->load->view('admin/includes/Admin_header', $data);
    $this->load->view('admin/User');
    $this->load->view('admin/includes/Admin_footer');

  }
  function address()
  {
    $this->check_admin();
    $data['title'] = 'Ether Address';
    $query         = $this->_custom_query("SELECT user.id,user.name,user.email,address.eth_add,address.user_id
      FROM user
      INNER JOIN  address ON user.id =address.user_id");

      $data['rows'] = $query->result();

      $this->load->view('admin/includes/Admin_header', $data);
      $this->load->view('admin/Address');
      $this->load->view('admin/includes/Admin_footer');
    }
    function transaction()
    {
      $this->check_admin();

      $query = $this->_custom_query("SELECT user.id,user.name,user.email,transaction.address,transaction.amount,transaction.type,transaction.date,transaction.token,transaction.user_id,transaction.status
        FROM user
        INNER JOIN  transaction ON user.id =transaction.user_id where transaction.status = 1");

        $data['rows']  = $query->result();
        $data['title'] = 'Transaction';
        $this->load->view('admin/includes/Admin_header', $data);
        $this->load->view('admin/Transaction');
        $this->load->view('admin/includes/Admin_footer');
      }
      function rate(){
        $this->check_admin();
        $token_price = $this->input->post('token_price',true);
        if(!empty($token_price))
        {
          $return = $this->_custom_query("select * from rate");
          if(count($return->result()) == 1)
          {
            //update
            $this->_custom_query("update rate set eth_value = '$token_price' where id = 1");
            redirect(base_url('admin/rate'));
          }
          else
          {
            //insert
            $this->_custom_query("insert into rate (eth_value) values ('$token_price')");
            redirect(base_url('admin/rate'));
          }
        }
        $return = $this->_custom_query("select * from rate");
        if(count($return->result()) == 1)
        {
          $data['rate'] = $return->result()[0]->eth_value;
        }
        $data['title'] = 'Rate';
        $query         = $this->_custom_query("SELECT * FROM rate");
        $data['rows']  = $query->result();
        $this->load->view('admin/includes/Admin_header', $data);
        $this->load->view('admin/Rate');
        $this->load->view('admin/includes/Admin_footer');

      }
      function check_admin()
      {
        if ($this->session->userdata('id') == '' && $this->session->userdata('email') == '') {
          redirect('Admin');
        }
      }

      function logout()
      {
        unset($_SESSION['id']);
        unset($_SESSION['email']);
        $this->session->sess_destroy();
        redirect(base_url('admin'));
      }


    }
